'use strict';

const version = "1.0.0";
const stackConfigLocation = "stack.json";
const systemConfigLocation = "system.json";
const userConfigLocation = "user.json";

const os = require('os');
const fs = require('fs');
const path = require('path');
const crypto = require('crypto');
const tar = require('tar-fs');
const prettyBytes = require('pretty-bytes');
require('colors');
const Client = require('ssh2').Client;
const PropertiesReader = require('properties-reader');
const {
  SHA3
} = require('sha3');

console.info((`LeanGate Installer v${version} (C) 2019 Leonid Fedotov`).cyan);
console.log("Started at " + new Date());

class Stack {

  constructor(inventory) {
    if (typeof inventory === "object") {

      this.nodes = inventory["nodes"];
      this.local = false;

      // Calculate first time to not calculate later
      this.maxAddressSize = 0;
      for (let i = 0; i < this.nodes.length; i++)
        if (this.nodes[i]["address"].length > this.maxAddressSize)
          this.maxAddressSize = this.nodes[i]["address"].length;

    } else {
      this.local = true;
      this.nodes = [{
        "address": "localhost",
        "username": os.userInfo().username
      }]
      this.maxAddressSize = "localhost".length;
    }
  }

  upload(files) {
    const maxNodeAddressSize = this.maxAddressSize;
    let promises = [];
    for (let i = 0; i < this.nodes.length; i++) {
      const node = this.nodes[i];
      promises.push(new Promise((resolve, reject) => {
        node.connection.sftp(function(err, sftp) {
          if (err) reject(err);
          let promises2 = [];
          for (let j = 0; j < files.length; j++) {
            promises2.push(new Promise((resolve2, reject2) => {
              sftp.fastPut(files[j]["remote"], files[j]["local"], (err) => {
                if (err) return reject2(err);
                resolve2();
              });
            }));
          }
          Promise.all(promises2).then(() => {
            console.log(
              node["address"] +
              " ".repeat(maxNodeAddressSize - node["address"].length + 2) +
              "[" + "OK".green + "]");
            resolve();
          }).catch((err) => {
            console.error(
              node["address"] +
              " ".repeat(maxNodeAddressSize - node["address"].length + 2) +
              "[" + "FAIL".red + "]");
            reject(err);
          });
        });
      }));
    }
    return Promise.all(promises);
  }

  async
    do(describe, action, resolver, doNotFailOnError) {

      let self = this;

      if (typeof doNotFailOnError !== "boolean")
        doNotFailOnError = false;

      console.log(`> ${describe}`);

      if (!this.local) {
        let promises = [];

        for (let i = 0; i < this.nodes.length; i++) {
          const node = this.nodes[i];
          promises.push(new Promise((resolve, reject) => {
            node["connection"].exec(action, function(err, stream) {
              if (err) return reject(err);
              let stdout = "";
              let stderr = "";
              stream.on('close', function(code, signal) {
                let problem = false;
                try {
                  resolver(
                    node,
                    code,
                    signal,
                    stdout.replace(/\n$/, ""),
                    stderr.replace(/\n$/, "")
                  );
                } catch (err) {
                  problem = true;
                  if (!doNotFailOnError) {
                    console.error(node["address"] +
                      " ".repeat(self.maxAddressSize - node["address"].length + 2) +
                      "[" + "FAIL".red + "]");
                    return reject(err);
                  }
                }
                console.log(node["address"] +
                  " ".repeat(self.maxAddressSize - node["address"].length + 2) +
                  "[" + (problem ? "FAIL".yellow : "OK".green) + "]");
                resolve();
              }).on('data', function(data) {
                stdout += data;
              }).stderr.on('data', function(data) {
                stderr += data;
              });
            });
          }));
        }

        return Promise.all(promises);

      } else {

        // TODO: fix fake code and signal
        let code = 0;
        let signal = "";
        try {
          const {
            stdout,
            stderr
          } = exec(action, {
            shell: true
          });
        } catch (err) {
          code = 1
        }

        let problem = false;
        try {
          resolver(
            this.nodes[0],
            code,
            signal,
            typeof stdout !== "string" ? "" : stdout.replace(/\n$/, ""),
            typeof stderr !== "string" ? "" : stderr.replace(/\n$/, "")
          );
        } catch (err) {
          problem = true;
          if (!doNotFailOnError) {
            console.error("localhost" +
              " ".repeat(2) +
              "[" + "FAIL".red + "]");
            throw err;
          }
        }
        console.log("localhost" +
          " ".repeat(2) +
          "[" + (problem ? "FAIL".yellow : "OK".green) + "]");

      }

    }

    async disconnect() {
      const self = this;
      if (!this.local) this.nodes.forEach(function(node) {
        node["connection"].end();
        console.debug(node["address"] +
          " ".repeat(self.maxAddressSize - node["address"].length + 2) +
          "[" + ("DISCONNECTED".green) + "]");
      });
    }

}

const failInstallation = (err) => {
  if (typeof err !== "undefined") {
    throw err;
  } else {
    throw new Error("Installation failure");
  }
}

const isFileExists = (fileName) => {
  return new Promise((resolve) => {
    fs.access(fileName, fs.constants.F_OK, (err) => {
      resolve(!err);
    });
  });
}

const readFile = (fileName) => {
  return new Promise((resolve, reject) => {
    fs.readFile(fileName, (err, data) => {
      if (err) return reject(err);
      resolve(data);
    });
  });
}

const deleteFile = (fileName) => {
  return new Promise((resolve, reject) => {
    fs.unlink(fileName, (err, data) => {
      if (err) return reject(err);
      resolve(data);
    });
  });
}

const getFileStat = (fileName) => {
  return new Promise((resolve, reject) => {
    fs.stat(fileName, (err, data) => {
      if (err) return reject(err);
      resolve(data);
    });
  });
}

const getFileSize = async (fileName) => {
  const rawStats = await getFileStat(fileName);
  return rawStats.size;
}

const getBeautyFileSize = async (fileName) => {
  const size = await getFileSize(fileName);
  return prettyBytes(size ? size : 0);
}

const readStackConfig = () => {
  return new Promise((resolve, reject) => {
    fs.access(stackConfigLocation, fs.constants.F_OK, (err) => {
      if (err) return reject("nofile");
      fs.readFile(stackConfigLocation, (err, data) => {
        if (err) return reject(err);
        try {
          let parsedConfig = JSON.parse(data);
          resolve(parsedConfig);
        } catch (err) {
          reject("invalidjson");
        }
      });
    });
  });
}

const readSystemConfig = () => {
  return new Promise((resolve, reject) => {
    fs.access(systemConfigLocation, fs.constants.F_OK, (err) => {
      if (err) return reject("nofile");
      fs.readFile(systemConfigLocation, (err, data) => {
        if (err) return reject(err);
        try {
          let parsedConfig = JSON.parse(data);
          resolve(parsedConfig);
        } catch (err) {
          reject("invalidjson");
        }
      });
    });
  });
}

const readUserConfig = () => {
  return new Promise((resolve, reject) => {
    fs.access(userConfigLocation, fs.constants.F_OK, (err) => {
      if (err) return reject("nofile");
      fs.readFile(userConfigLocation, (err, data) => {
        if (err) return reject(err);
        try {
          let parsedConfig = JSON.parse(data);
          resolve(parsedConfig);
        } catch (err) {
          reject("invalidjson");
        }
      });
    });
  });
}

const sleep = (time) => {
  return new Promise((resolve) => {
    setTimeout(_ => resolve(), time * 1000);
  });
};

const makeRandomBytes = (amount = 64) => {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(amount, function(err, buffer) {
      if (err) return reject(err);
      resolve(buffer.toString('hex'));
    });
  });
}

const prepareStack = async () => {

  let stack;
  try {
    stack = await readStackConfig();
  } catch (err) {
    if (err instanceof Error) {
      console.error("An unknown error thrown while loading cluster \
configuration file".red, err);
      failInstallation();
    } else if (err == "nofile") {
      return;
    } else if (err == "invalidjson") {
      console.error("Cluster configuration file can't be parsed".red);
      failInstallation();
    } else {
      console.error("Unknown software condition".red);
      failInstallation();
    }
  }

  console.log("STACK INVENTORY LOAD".bold);

  let nodesCount = typeof stack["nodes"] === "object" ?
    stack["nodes"].length : 0;
  console.debug("Found " + nodesCount + " host" + (nodesCount !== 1 ? "s" : ""));

  if (nodesCount < 2) {
    console.error("Nodes amout is less than 2. Stack installation is not \
possible.".red);
    failInstallation();
  }

  // Prepare keys for each host

  let keysLocationType;
  if (typeof stack["key"] === "string") { // Is there single key for all hosts?
    keysLocationType = "single";
  } else if (typeof stack["keys-directory"] ===
    "string") { // Is there all key at some directory?
    keysLocationType = "auto";
  } else { // Did user defined all keys manually for each node?
    for (let i = 0; i < nodesCount; i++) {
      if (typeof stack["nodes"][i] !== "object" ||
        typeof stack["nodes"][i]["key"] !== "string" ||
        stack["nodes"][i]["key"] !== "" ||
        typeof stack["nodes"][i]["address"] !== "string" ||
        stack["nodes"][i]["address"] !== "") keysLocationType = "invalid";
    }
    if (typeof keysLocationType === "undefined") keysLocationType = "manual";
  }

  console.log("Keys location type found: " +
    keysLocationType[(keysLocationType === "invalid" ? "red" : "green")]);
  if (keysLocationType === "invalid") failInstallation();

  let promises = [];
  let maxNodeAddressSize = 0;

  console.log("> Connecting to nodes");

  for (let i = 0; i < nodesCount; i++) {

    // Do format
    if (typeof stack["nodes"][i] === "string") {
      stack["nodes"][i] = {
        "address": stack["nodes"][i]
      }
    }
    if (stack["nodes"][i]["address"].length > maxNodeAddressSize)
      maxNodeAddressSize = stack["nodes"][i]["address"].length;

    switch (keysLocationType) {
      case "single":
        stack["nodes"][i]["key"] = stack["key"];
        break;
      case "auto":
        stack["nodes"][i]["key"] = path.format({
          dir: stack["keys-directory"],
          name: stack["nodes"][i],
          ext: ".key"
        });
        break;
    }

    // check port definition and fill default port if not defined
    if (typeof stack["nodes"][i]["port"] !== "string" &&
      typeof stack["nodes"][i]["port"] !== "number") {
      stack["nodes"][i]["port"] = 22;
    } else if (typeof stack["nodes"][i]["port"] === "string") {
      stack["nodes"][i]["port"] = parseInt(stack["nodes"][i]["port"]);
      if (isNaN(stack["nodes"][i]["port"]))
        console.error(`Node's ${stack["nodes"][i]["address"]} port is not a \
number`.red);
      failInstallation();
    }

    // check username definition and fill default username if not defined
    if (typeof stack["nodes"][i]["username"] !== "string") {
      stack["nodes"][i]["username"] = "root";
    }

    // check keys files exists
    if (!await isFileExists(stack["nodes"][i]["key"])) {
      console.error(`Node's ${stack["nodes"][i]["address"]} key file \
"${stack["nodes"][i]["key"]}" do not exists`.red);
      failInstallation();
    }

    stack["nodes"][i]["connection"] = new Client();

    let keyData = await readFile(stack["nodes"][i]["key"]);

    promises.push(new Promise((resolve, reject) => {
      stack["nodes"][i]["connection"].on("ready", function() {
        console.log(
          stack["nodes"][i]["address"] +
          " ".repeat(maxNodeAddressSize -
            stack["nodes"][i]["address"].length + 2) +
          "[" + "CONNECTED".green + "]");
        resolve();
      }).on('error', function(err) {
        console.log(stack["nodes"][i]["address"] + " [" + "CONNECTION \
FAILURE".red + "]");
        reject(err);
      }).connect({
        host: stack["nodes"][i]["address"],
        port: 22,
        username: stack["nodes"][i]["username"],
        privateKey: keyData
      });
    }));

  }

  try {
    await Promise.all(promises);
  } catch (err) {
    console.error("Some nodes are not available. Try to connect to them \
manually and start installation again.".red);
    failInstallation(err);
  }
  console.info("All nodes are available".green);

  let balancersExpected = nodesCount < 100 ? 1 : 2;
  //let mastersExpected = nodesCount - balancersExpected;

  let balancers = 0;
  let masters = 0;

  // Is roles already defined? Let's count

  for (let i = 0; i < nodesCount; i++) {
    if (typeof stack["nodes"][i]["role"] === "string") {
      switch (stack["nodes"][i]["role"]) {
        case "balancer":
          balancers++;
          break;
        case "master":
          masters++;
          break;
        default:
          console.error(`Unknown role "${stack["nodes"][i]["role"]}" specified \
for node ${stack["nodes"][i]}`.red);
          failInstallation();
      }
    }
  }

  // Define roles according to expected values
  console.log("> Nodes roles definition");

  for (let i = 0; i < nodesCount; i++) {
    if (typeof stack["nodes"][i]["role"] !== "string") {
      if (balancers < balancersExpected) {
        stack["nodes"][i]["role"] = "balancer";
        balancers++;
      } else {
        stack["nodes"][i]["role"] = "master";
        masters++;
      }
    }
    console.log(
      stack["nodes"][i]["address"] +
      " ".repeat(maxNodeAddressSize - stack["nodes"][i]["address"].length + 2) +
      "[" +
      stack["nodes"][i]["role"].toUpperCase().magenta +
      "]");
  }

  if (balancers === 0) {
    console.error("There are no balancers. Please fix your inventory file.".red);
    failInstallation();
  }

  if (masters === 0) {
    console.error("There are no masters. Please fix your inventory file.".red);
    failInstallation();
  }

  console.log(`Balancers: ${balancers}, Masters: ${masters}`);
  console.info("All roles defined correctly".green)

  return stack;

}

const installSystem = async (pushUpdate) => {

  const installStartDate = new Date();

  let systemConfig;
  try {
    systemConfig = await readSystemConfig();
  } catch (err) {
    if (err instanceof Error) {
      console.error("An unknown error thrown while loading system \
configuration file".red, err);
    } else if (err == "nofile") {
      console.error("System configuration file not found".red);
    } else if (err == "invalidjson") {
      console.error("System configuration file can't be parsed".red);
    } else {
      console.error("Unknown software condition".red);
    }
    failInstallation();
  }

  let userConfig;
  try {
    userConfig = await readUserConfig();
  } catch (err) {
    if (err instanceof Error) {
      console.error("An unknown error thrown while loading system \
  configuration file".red, err);
    } else if (err == "nofile") {
      userConfig = {};
    } else if (err == "invalidjson") {
      console.error("User configuration file can't be parsed".red);
    } else {
      console.error("Unknown software condition".red);
    }
    failInstallation();
  }

  if (typeof userConfig["id"] !== "number") {
    userConfig["id"] = 1;
  }

  if (typeof userConfig["secret"] !== "string") {

    const password = Math.random().toString(36).substring(2); // 12-length pass

    const hash = new SHA3(512);
    hash.update(password);

    userConfig["secret"] = hash.digest('hex');
    userConfig["secretAlgorithm"] = 1;

    try {
      await fs.promises.writeFile("USERPASSWORD", password);
    } catch (err) {
      console.error("Can't write password file");
      throw err;
    }

  }

  if (typeof userConfig["created"] !== "number") {
    userConfig["created"] = new Date().getTime();
  }

  if (typeof userConfig["updated"] !== "number") {
    userConfig["updated"] = new Date().getTime();
  }

  if (typeof userConfig["username"] !== "string") {
    userConfig["username"] = "admin";
  }

  if (typeof userConfig["firstname"] !== "string") {
    userConfig["firstname"] = "Users";
  }

  if (typeof userConfig["lastname"] !== "string") {
    userConfig["lastname"] = "Admin";
  }

  if (typeof userConfig["sex"] !== "number") {
    userConfig["sex"] = 0;
  }

  if (typeof userConfig["type"] !== "number") {
    userConfig["type"] = 0;
  }

  if (typeof userConfig["grants"] !== "object") {
    userConfig["grants"] = {
      "super": true
    };
  }

  let stackInventory = await prepareStack();
  let stack = new Stack(stackInventory);

  if (pushUpdate) {
    console.log("PUSH UPDATE".bold);

    if (!stack.local) {

      const systemIndexFile = path.format({
        dir: pushUpdate,
        base: "index.js"
      });
      if (!isFileExists(systemIndexFile)) {
        throw new Error("Can't find system index file " + systemIndexFile);
      }

      console.log("> Packing update");
      tar.pack(pushUpdate, {
        ignore: function(name) {
          return name.indexOf("node_modules") !== -1;
        }
      }).pipe(fs.createWriteStream('/tmp/leangate-update.tar'));

      // there is a bug - file saved not immediatly
      await sleep(.1);

      const beautySize = await getBeautyFileSize("/tmp/leangate-update.tar");
      console.info(`Update packed: ${beautySize}`.green);

      await stack.do(
        "Stop system",
        "systemctl stop leangate",
        (_node, code, _signal, stdout, stderr) => {
          if (code !== 0) {
            throw new Error(`Failed to stop sustem \n${stdout}\n${stderr}`);
          }
        });
      console.info("System stopped".green);

      console.log("> Upload update");
      await stack.upload([{
        local: "/tmp/leangate-update.tar",
        remote: "/tmp/leangate-update.tar",
        overwrite: true
      }]);
      console.info("Update uploaded".green)

      await deleteFile("/tmp/leangate-update.tar");

      await stack.do(
        "Install update",
        "rm -rf /etc/leangate/system/* && \
         tar -xf /tmp/leangate-update.tar -C /etc/leangate/system/ && \
         rm -f /tmp/leangate-update.tar && \
         rm -rf /etc/leangate/system/node_modules",
        (_node, code, _signal, stdout, stderr) => {
          if (code !== 0) {
            throw new Error(`Failed to install update \n${stdout}\n${stderr}`);
          }
        });
      console.info("Update installed".green);

      await stack.do(
        "Start system",
        "systemctl start leangate",
        (_node, code, _signal, stdout, stderr) => {
          if (code !== 0) {
            throw new Error(`Failed to start sustem \n${stdout}\n${stderr}`);
          }
        });
      console.info("System started".green);

      // some nodes can be slow
      await sleep(1);

    } else {
      console.log("Push to local system is not supported");
    }

  } else {

    console.log("ENVIRONMENT PREPARATION".bold);

    // Are nodes running on Debian/Ubuntu?
    await stack.do(
      "Check node's operating system",
      "cat /etc/os-release",
      (node, _code, _signal, stdout, _stderr) => {
        const properties = PropertiesReader().read(stdout);

        let osID = properties.get('ID') ? properties.get('ID') : "unknown os";
        // Do replace invisible symbols from versions
        let osVersion = properties.get('VERSION_ID') ?
          parseInt(properties.get('VERSION_ID').replace(/\D+/g, "")) :
          null;
        console.log(
          node["address"] +
          " ".repeat(stack.maxAddressSize - node["address"].length + 2) +
          "[" + (
            osID + (osVersion ? ` ${osVersion}` : "")
          ).toUpperCase().magenta + "]");
        if (osID === "debian") {
          if (osVersion < 9) {
            throw new Error("Debian version is lower than required.");
          }
        } else if (osID === "ubuntu") {
          if (osVersion < 18) {
            throw new Error("Ubuntu version is lower than required.");
          }
        } else {
          throw new Error("OS distro is unknown.");
        }
      });
    console.info("All OS distribs are OK".green)

    // Are we sudouers?
    await stack.do(
      "Check node's user is root",
      "echo ${EUID}",
      (node, _code, _signal, stdout, _stderr) => {
        if (stdout !== "0") { // if node's user is not root
          throw new Error(`User "${node["username"]}" is not a sudoer. \
      EUID=${stdout}, but 0 expected.`);
        }
      });
    console.info("All nodes are available under root".green)

    // Let's check external network connection from nodes
    let pingAddress = "1.1.1.1";
    let pingAttemts = 4;
    let unstable = false;
    await stack.do(
      "Check access to external network",
      `ping -c ${pingAttemts} ${pingAddress}`,
      (_node, _code, _signal, stdout, _stderr) => {
        if (stdout.indexOf("100% packet loss") !== -1) { // if no internet
          throw new Error(`Ping ${pingAddress} failed, is network available?`);
        } else if (stdout.indexOf("0.0% packet loss") !== -1) {
          unstable = true;
        }
      });
    console.info("All nodes are online".green)
    if (unstable) {
      console.warn("WARNING! External network is unstable.".yellow);
    }

    // Let's upgrade system packages
    await stack.do(
      "Upgrade node's host system packages (this can take some time)",
      "apt update && apt upgrade -y",
      (_node, code, _signal, stdout, stderr) => {
        if (code !== 0) {
          throw new Error(`Failed to upgrade system packages \
\n${stdout}\n${stderr}`);
        }
      });
    console.info("All system packages were upgraded".green)

    // Install base packages
    await stack.do(
      "Base packages installation",
      "apt install -y apt-transport-https \
                      ca-certificates \
                      curl \
                      software-properties-common",
      (_node, code, _signal, stdout, stderr) => {
        if (code !== 0) {
          throw new Error(`Failed to install base backages \
\n${stdout}\n${stderr}`);
        }
      });
    console.info("All base packages were installed".green)

    // Install GPG keys
    await stack.do(
      "GPG keys installation",
      "wget --quiet -O - https://download.docker.com/linux/debian/gpg \
| sudo apt-key add - &&\
      wget --quiet -O - https://deb.nodesource.com/gpgkey/nodesource.gpg.key \
| sudo apt-key add -",
      (_node, code, _signal, stdout, stderr) => {
        if (code !== 0) {
          throw new Error(`Failed to install GPG keys \n${stdout}\n${stderr}`);
        }
      });
    console.info("GPG keys were installed".green)

    // Add additional repos

    // TODO: this is unreadable
    await stack.do(
      "Add additional repositories",
      "echo \"deb [arch=amd64] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable\" | \
  sudo tee -a /etc/apt/sources.list.d/docker.list && \
  echo \"deb https://deb.nodesource.com/node_12.x $(lsb_release -cs) main\" | \
  sudo tee /etc/apt/sources.list.d/nodesource.list && \
  echo \"deb-src https://deb.nodesource.com/node_12.x $(lsb_release -cs) main\"\
  | sudo tee -a /etc/apt/sources.list.d/nodesource.list && \
  apt update",
      (_node, code, _signal, stdout, stderr) => {
        if (code !== 0) {
          throw new Error(`Failed to add repos \n${stdout}\n${stderr}`);
        }
      });
    console.info("Additional repos were added".green)

    // Install Git
    await stack.do(
      "Git installation",
      "apt install git -y",
      (_node, code, _signal, stdout, stderr) => {
        if (code !== 0) {
          throw new Error(`Failed to install Git \n${stdout}\n${stderr}`);
        }
      });
    console.info("Git installed".green)

    // Install Docker
    await stack.do(
      "Docker installation",
      "apt install docker-ce -y",
      (_node, code, _signal, stdout, stderr) => {
        if (code !== 0) {
          throw new Error(`Failed to install Docker \n${stdout}\n${stderr}`);
        }
      });
    console.info("Docker installed".green)

    // Start Docker
    await stack.do(
      "Docker start",
      "systemctl enable docker && systemctl start docker",
      (_node, code, _signal, stdout, stderr) => {
        if (code !== 0) {
          throw new Error(`Failed to start Docker \n${stdout}\n${stderr}`);
        }
      });
    console.info("Docker started".green);

    // Install Node.js
    await stack.do(
      "Node.js installation",
      "apt install nodejs -y",
      (_node, code, _signal, stdout, stderr) => {
        if (code !== 0) {
          throw new Error(`Failed to install Node.js \n${stdout}\n${stderr}`);
        }
      });
    console.info("Node.js installed".green);

    // TODO: check docker prover version installed
    // TODO: check nodejs proper version installed
    // TODO: check npm proper version installed
    // TODO: check git proper version installed

    console.log("SYSTEM INSTALLATION".bold);

    let systemRelease = process.env["LEANGATE_RELEASE"];
    if (typeof systemRelease === "undefined" ||
      !systemRelease ||
      systemRelease === "") {
      console.warn("WARNING! There is no release version environment \
variable.".yellow);
      systemRelease = "master";
    }

    let nothingToStop = false;
    let systemAlreadyStopped = false;
    await stack.do(
      "Stop running system",
      `(systemctl disable leangate || true) && \
       (systemctl kill leangate || true)`,
      (_node, code, _signal, stdout, stderr) => {
        if (code !== 0) {
          if (stderr.indexOf("Failed to disable unit") !== -1) {
            nothingToStop = true;
          } else if (stderr.indexOf("Unit leangate.service not loaded") !==
            -1) {
            systemAlreadyStopped = true;
          } else {
            throw new Error(`Failed to stop system \n${stdout}\n${stderr}`);
          }
        }
      });
    if (nothingToStop) {
      console.info("System is not installed yet, nothing to stop".green);
    } else if (systemAlreadyStopped) {
      console.info("System is already stopped".green);
    } else {
      console.info("System stopped".green);
    }

    let containersExists = false;
    await stack.do(
      "Check running Docker containers",
      `docker ps -q | wc -l`,
      (_node, code, _signal, stdout, stderr) => {
        if (stdout !== "" && parseInt(stdout) > 0) {
          containersExists = true;
        } else if (code !== 0) {
          throw new Error(`Failed to stop Docker containers \
\n${stdout}\n${stderr}`);
        }
      });
    console.info(`Docker containers were ${
      containersExists ? "" : "NOT "
    }found`.green);

    if (containersExists) {
      await stack.do(
        "Stop Docker containers",
        `docker stop $(docker ps -a -q) || true`,
        (_node, code, _signal, stdout, stderr) => {
          if (code !== 0) {
            throw new Error(`Failed to stop Docker containers \
\n${stdout}\n${stderr}`);
          }
        });
      console.info("Docker containers stopped".green);
    }

    // todo: move to config
    const skipCleanup = true;

    if (!skipCleanup) {
      await stack.do(
        "Cleanup Docker",
        `(docker rm $(docker ps -a -q) || true) && \
       (docker rmi $(docker images -q) || true) && \
       docker system prune -a -f`,
        (_node, code, _signal, stdout, stderr) => {
          if (code !== 0) {
            throw new Error(`Failed to cleanup docker \
\n${stdout}\n${stderr}`);
          }
        });
      console.info("Docker cleanuped".green);
    }

    await stack.do(
      "Download and unpack system sources from " + systemRelease.magenta + " \
release",
      `rm -rf /etc/leangate && \
      rm -rf /tmp/leangate-* && \
      rm -rf /usr/share/leangate/system/logs && \
      wget --quiet -O /tmp/leangate-${systemRelease}.tar.bz2 \
      https://gitlab.com/xterra/leangate/-/archive/${systemRelease}/leangate-${systemRelease}.tar.bz2 \
      && tar xvjf /tmp/leangate-${systemRelease}.tar.bz2 -C /tmp \
      && mv /tmp/leangate-${systemRelease} /etc/leangate \
      && chmod +x /etc/leangate/start.sh \
      && rm -f /tmp/leangate-${systemRelease}.tar.bz2 \
      && mkdir -p /usr/share/leangate \
      && chmod 777 /usr/share/leangate \
      && rm -rf /usr/share/leangate/files \
      && mkdir /usr/share/leangate/files \
      && chown 1000 /usr/share/leangate/files \
      && chmod 777 /usr/share/leangate/files \
      && rm -rf /usr/share/leangate/sessions`,
      (_node, code, _signal, stdout, stderr) => {
        if (code !== 0) {
          throw new Error(`Failed to install system sources \
\n${stdout}\n${stderr}`);
        }
      });
    console.info("System sources downloaded".green);

    let publicNodesInventory = [];
    if (typeof stackInventory !== "undefined") {
      for (let i = 0; i < stackInventory.nodes.length; i++) {
        publicNodesInventory.push({
          address: stackInventory.nodes[i].address,
          role: stackInventory.nodes[i].role
        });
      }
    }

    await stack.do(
      "Copy configurations",
      `cat <<LEOT >> /etc/leangate/configs/system.json\n${JSON.stringify(systemConfig)}\nLEOT\n
      cat <<LEOT >> /etc/leangate/configs/secret.key\n${await makeRandomBytes(256)}\nLEOT\n` +
      (
        typeof stackInventory !== "undefined" ?
        `cat <<LEOT >> /etc/leangate/configs/nodes.json\n${JSON.stringify(publicNodesInventory)}\nLEOT\n` :
        ""
      ),
      (_node, code, _signal, stdout, stderr) => {
        if (code !== 0) {
          throw new Error(`Failed to copy configurations \
\n${stdout}\n${stderr}`);
        }
      });
    console.info("Configurations are copied".green);

    await stack.do(
      "Install applications",
      "rm -rf /usr/share/leangate/apps && mv /etc/leangate/apps /usr/share/leangate",
      (_node, code, _signal, stdout, stderr) => {
        if (code !== 0) {
          throw new Error(`Failed to install applications \
\n${stdout}\n${stderr}`);
        }
      });
    console.info("Applications were installed".green);

    await stack.do(
      "Create user",
      `rm -rf /usr/share/leangate/users/${userConfig.id}
      mkdir -p /usr/share/leangate/users/${userConfig.id}
      cat <<LEOT >> /usr/share/leangate/users/${userConfig.id}/account.json\n${JSON.stringify(userConfig)}\nLEOT\n
      chown 2001:2000 /usr/share/leangate/users/${userConfig.id}/account.json`,
      (_node, code, _signal, stdout, stderr) => {
        if (code !== 0) {
          throw new Error(`Failed to create user \
\n${stdout}\n${stderr}`);
        }
      });
    console.info("User created".green);

    await stack.do(
      "Install system service",
      `mv /etc/leangate/leangate.service /etc/systemd/system/leangate.service \
      && systemctl daemon-reload \
      && systemctl enable leangate \
      && systemctl start leangate`,
      (_node, code, _signal, stdout, stderr) => {
        if (code !== 0) {
          throw new Error(`Failed to install system service \
\n${stdout}\n${stderr}`);
        }
      });
    console.info("System service installed".green);

  }

  sleep(5);

  await stack.do(
    "Check system service status",
    `systemctl is-active leangate`,
    (_node, _code, _signal, stdout, stderr) => {
      if (stdout !== "active") {
        throw new Error(`System service is inactive \n${stdout}\n${stderr}`);
      }
    });
  console.info("System service is active".green);

  const iterateSeconds = 3;
  let waitSeconds = 1200;
  let alive = false;
  let failed = false;

  while (!failed && !alive && waitSeconds > 0) {
    console.log(`Waiting ${(waitSeconds)} second${((waitSeconds)===1)?"":"s"} \
until system up...`);
    await sleep(iterateSeconds);
    await stack.do(
      "Check system state",
      "cat /etc/leangate/STATE",
      (_node, code, _signal, stdout, stderr) => {
        if (stdout === "HEALTHY") {
          alive = true;
        } else if (stdout === "BROKEN" ||
          stdout === "FAILED" ||
          stdout === "DOWN") {
          failed = true;
        }
      });
    waitSeconds -= iterateSeconds;
  }

  console.log("> Disconnecting from nodes:");
  stack.disconnect();

  if (!alive) {
    console.error(`FAILURE: ${failed ? "SYSTEM BROKEN" : "TIMEOUT"}`.red);
  } else {
    console.info("SUCCESS!".green);
  }
  console.log(`\nInstallation done in ${beautifyTimeDifference(installStartDate,
                                                               new Date())}`);

}

const beautifyTimeDifference = (from, to) => {
  let beautyTimeDifference = "";
  const differenceMilliseconds = to - from;
  const differenceMinutes = Math.floor(differenceMilliseconds / 1000 / 60);
  const differenceSecondsWithoutMinutes = (
    Math.floor(differenceMilliseconds / 1000 - differenceMinutes * 60) * 10
  ) / 10;
  if (differenceMinutes > 0) {
    beautyTimeDifference += `${differenceMinutes} minute\
${differenceMinutes !== 1 ? "s" : ""} `;
  }

  beautyTimeDifference += `${differenceSecondsWithoutMinutes} second\
${differenceSecondsWithoutMinutes !== 1 ? "s" : ""}`;

  return beautyTimeDifference;
}

let pushUpdate = null;
for (let j = 0; j < process.argv.length; j++) {
  const splittedArgument = process.argv[j].split("=");
  if (splittedArgument[0] === "pushUpdate") {
    pushUpdate = splittedArgument[1];
  }
}

installSystem(pushUpdate).catch(err => {
  console.error(err);
  process.exit(1);
});

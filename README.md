# LeanGate Installer

A LeanGate installation one-click tool on physical compute machines.

## Installation types

There are 2 installation types:

1. [Single](#single-installation) - system will be installed on one computer.
This type of installation is suitable for home computers.
2. [Stack](#stack-installation) - system will be installed on swarm of
computers. This type of installation is suitable for enterprise users.

You have to desire what type of installation you need, there is no way to
convert installed system to another type.

## Single installation

### Prerequisites

Minimal requirements:
- [Debian 9](https://www.debian.org/distrib/) or [Ubuntu 18](https://www.ubuntu.com/download/server)
- Configured access to Internet, connected to computer
- Access to Internet without restrictions
- RAM 500Gb or more
- Free space 5Gb or more

Requirements for comfortable usage:
- 16Gb RAM
- Quad-core Intel CPU with Hyper-Threading technology
- NVIDIA Graphics card identical GTX 1060
- SSD 250Gb as primary disk, HDD 2TB as secondary disk
- Unlimited network bandwidth
- Network download/upload 100MBi/s

### Installation

1. Switch to root, you will be asked to enter root-password:
   ```
   su root
   ```
2. Cleanup temporary directory:
   ```
   rm -rf /tmp/leangate-installer*
   ```
3. Download Installer Git-repository to your computer:
   ```
   wget -O /tmp/leangate-installer.tar.bz2 https://gitlab.com/xterra/leangate-installer/-/archive/master/leangate-installer-master.tar.bz2
   ```
4. Unpack downloaded archive:
   ```
   tar xvjf /tmp/leangate-installer.tar.bz2 -C /tmp
   ```
5. Run LeanGate installation:
   ```
   bash /tmp/leangate-installer-*/install.sh
   ```

## Stack installation

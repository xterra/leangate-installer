#!/usr/bin/env bash

set -e

cd $(dirname "$0")

LEANGATE_RELEASE="master"

do_fail () {
  echo "Installation failed. Please fix installation problems according to \
logs below and try again."
  exit 1
}

if [ "$EUID" -ne 0 ]; then
  echo -e "Running as root [\033[31mNO\033[0m]"
  echo "You have to execute installation script as root. Try \"sudo su\" \
command and rerun installation script."
  do_fail
else
  echo -e "Running as root [\033[32mYES\033[0m]"
fi

if [[ $(which node 2>/dev/null) ]]; then

  # parse Node.js versions
  node_version=$(node -v | cut -dv -f2)
  major_node_version=$(echo $node_version | cut -d. -f1)
  minor_node_version=$(echo $node_version | cut -d. -f2)

  # Node.js version should be 12.2+
  if (( major_node_version > 12 )) ||
     (
        [[ "${major_node_version}" == "12" ]] &&
        (( minor_node_version > 1 ))
     ); then
    echo -e "Node.js ${node_version} [\033[32mOK\033[0m]"
  else
    echo -e "Node.js ${node_version} [\033[31mINVALID VERSION\033[0m]"
    echo "Please install Node.js v12.2+"
    do_fail
  fi

else

  echo -n "Installing Node.js... "

  curl -sL https://deb.nodesource.com/setup_12.x | bash - > /dev/null 2>&1 && \
  apt-get install -y nodejs > /dev/null 2>&1 || echo -e "[\033[32mFAILED\
  \033[0m]" && do_fail

  echo -e "[\033[32mINSTALLED\033[0m]"

fi

if [[ $(which npm 2>/dev/null) ]]; then

  # parse NPM versions
  npm_version=$(npm -v)
  major_npm_version=$(echo $npm_version | cut -d. -f1)
  minor_npm_version=$(echo $npm_version | cut -d. -f2)

  # NPM version should be 6.9+
  if (( major_npm_version > 6 )) ||
     (
        [[ "${major_npm_version}" == "6" ]] &&
        (( minor_npm_version > 8 ))
     ); then
    echo -e "NPM ${npm_version} [\033[32mOK\033[0m]"
  else
    echo -e "NPM ${npm_version} [\033[31mINVALID VERSION\033[0m]"
    echo "Please install NPM v6.9+"
    do_fail
  fi

else

  echo -e "NPM [\033[31mNOT INSTALLED\033[0m]"
  echo "NPM should be installed on Node.js installation. Reinstall Node.js or \
fix the absence of NPM binary manually."
  do_fail

fi

echo -n "Install NPM packages "
npm install > /dev/null 2>&1 && echo -e "[\033[32mOK\033[0m]" || \
  ( echo -e "[\033[31mFAIL\033[0m]" && \
  echo "Execute \"npm install\" to see installation error" && do_fail )

echo -n "NPM cleanup "
npm prune > /dev/null 2>&1  && echo -e "[\033[32mOK\033[0m]" || \
  (echo -e "[\033[33mFAIL\033[0m]")

mkdir -p logs

echo "Starting installer..."
npm start 2>&1 | tee logs/installation.log || \
  ( echo -e "\033[31mInstaller execution failed, see installation.log file \
\033[0m" && exit 1 )
